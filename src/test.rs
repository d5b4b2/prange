use crate::*;

fn test(input: &str, expected: Result<&[u16], &str>) {
    let actual = parse(input)
        .map(|ranges| ranges.take(100).collect::<Vec<_>>())
        .map_err(|error| error.to_string());
    let actual = actual.as_deref().map_err(|x| x.as_str());
    assert_eq!(expected, actual);
}

#[test]
fn parse_empty() {
    test("", Ok(&[]));
}

#[test]
fn parse_one() {
    test("1", Ok(&[1]));
}

#[test]
fn parse_three() {
    test("1,5,13", Ok(&[1, 5, 13]));
}

#[test]
fn parse_one_range() {
    test("1-3", Ok(&[1, 2, 3]));
}

#[test]
fn parse_three_range() {
    test("1-3,4-6,8-9", Ok(&[1, 2, 3, 4, 5, 6, 8, 9]));
}

#[test]
fn parse_one_mixed() {
    test("1-3,5", Ok(&[1, 2, 3, 5]));
}

#[test]
fn parse_three_mixed() {
    test(
        "13,1-3,5,22-24,11-12,9",
        Ok(&[13, 1, 2, 3, 5, 22, 23, 24, 11, 12, 9]),
    );
}

#[test]
fn parse_intersection() {
    test("1-3,2-4", Ok(&[1, 2, 3, 2, 3, 4]));
}

#[test]
fn parse_open_left() {
    test("-3", Ok(&[1, 2, 3]));
}

#[test]
fn parse_open_right() {
    test("65533-", Ok(&[65533, 65534, 65535]));
}

#[test]
fn parse_reverse_range() {
    test("3-1", Ok(&[]));
}

#[test]
fn parse_err_illegal_char1() {
    test(
        "a",
        Err("Input contains an invalid character `a` at position 0"),
    );
}

#[test]
fn parse_err_illegal_char2() {
    test(
        "1-3,b",
        Err("Input contains an invalid character `b` at position 4"),
    );
}

#[test]
fn parse_err_integer() {
    test(
        "65536",
        Err("Input contains an invalid number `65536` at position 5"),
    );
}

#[test]
fn parse_err_empty_range1() {
    test(",", Err("Input contains an empty range at position 0"));
}

#[test]
fn parse_err_empty_range2() {
    test(",1", Err("Input contains an empty range at position 0"));
}

#[test]
fn parse_err_empty_range3() {
    test("1,", Err("Input contains an empty range at position 2"));
}

#[test]
fn parse_err_empty_range4() {
    test("1,,2", Err("Input contains an empty range at position 2"));
}

#[test]
fn parse_err_invalid_range1() {
    test("-", Err("Input contains an invalid range at position 1"));
}

#[test]
fn parse_err_invalid_range2() {
    test("1--2", Err("Input contains an invalid range at position 2"));
}

#[test]
fn parse_err_invalid_range3() {
    test("-1-2", Err("Input contains an invalid range at position 2"));
}

#[test]
fn parse_err_invalid_range4() {
    test("1-2-", Err("Input contains an invalid range at position 3"));
}
