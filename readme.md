# prange

Parse numeric ranges for indexing.

Inclusive-inclusive 1-based integer ranges. Parsed from strings.

## Examples

```
"2"         => [2]
"1-5"       => [1, 2, 3, 4, 5]
"1-3,5-6"   => [1, 2, 3, 5, 6]
"-3"        => [1, 2, 3]
"1-"        => [1, 2, 3, ..]
"1-3,2-4,7" => [1, 2, 3, 2, 3, 4, 7]
```
